# gateway

Main repo: https://gitlab.com/hardmlfinal/deploy

- Gateway converts text to embedding (USE model), determines the nearest cluster (4 clusters - 4 workers)  and send POST request to "faiss_worker"
- Gitlab-ci performs pull and deploy